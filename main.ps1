<#Функция "emailSend":
Формирует почтовое сообщение для отправки при помощи smtp.
Функции "emailSend" передают 2 параметра,
1. "subject" = "Connector errors". Берется из файла "settings.xml" в том же каталоге, что и скрипт.
2. "body". Это данные типа [string], которые передаются напрямую при вызове функции "emailSend".
#>
function emailSend {
    param (
        [string]$subject = $emailSettings["emailSubject"],
        [string]$body
    )
    #Write-Output $emailSettings;
    #Send-MailMessage -From $emailSettings["sendFrom"] -To $emailSettings["emails"].Split(";") -Subject $title -Body $body -SmtpServer $emailSettings["smtpHost"] -Port $emailSettings["smtpPort"] -Encoding UTF8
    
    <# создается пустой объект типа System.Net.Mail.MailMessage
    #>
    $mes = New-Object System.Net.Mail.MailMessage
    
    <#изменяет свойство From, объекта mes на значение $emailSettings["sendFrom"], то есть "nkryachko.ps@skbkontur.ru"
    #>
    $mes.From = $emailSettings["sendFrom"]

    <#для каждой из строк свойства  $emailSettings["emails"] выполнить разбивку по разделителю ";" и добавить их как отдельные строки "To"
    В данном примере адрес один, поэтому свойство объекта $mes будет иметь одно значение равное "kryachko.ps@skbkontur.ru"
    #>
    foreach ($mm in $emailSettings["emails"].Split(";")) {
        if ($mm) {
            $mes.To.Add($mm) 
        }
    }

    <#Замена значения свойства $mes.Subject на "Connector errors"#>
    $mes.Subject = $subject

    #$mes.IsBodyHTML = $true

    <#Изменение параметра $mes.Body на тот текст, который будет передан в качестве параметра для функции "emailSend"#>
    $mes.Body = $body


    <#Формирование объекта $smpt и назначение первых его свойств "Host" и "Port" в соответствии с данными из файла файла settings.xml. Host="smtp.kontur", Port=25#>
    $smtp = New-Object Net.Mail.SmtpClient($emailSettings["smtpHost"], $emailSettings["smtpPort"])
    <#Проверка значения свойства "needSSL"#>
    if ($emailSettings["needSSL"] -eq "1") {
        <#Если указанный выше свойство имеет значение 1, тогда присвоить свойству EnableSSL значение $True#>
        $smtp.EnableSSL = $true
    }
    <#Проверка значения свойства "needAuth"#>
    if ($emailSettings["needAuth"] -eq "1") {
        <#Если указанный выше свойство имеет значение 1, тогда присвоить свойству Credentials значения smtpUser = ничего и smtpPassword = ничего, то есть используется
        smtp relay без аутентификации, либо в данном примере удалены логин и пароль пользователя, хотя хранить их в открытом в виде .xml это такое себе занятие. #>
        $smtp.Credentials = New-Object System.Net.NetworkCredential($emailSettings["smtpUser"], $emailSettings["smtpPassword"]);
    }

    #$mes | Out-File -FilePath $logFile -Append
    <#конструкция вида try-catch, пытается отосласть электронное письмо через smt-relay smtp.kontur#>
    try {
        $smtp.Send($mes) 
    } catch {
        <#В случае какой-либо ошибки происходит изменения свойства "mailForError" с 1 на 0#>
        $emailSettings["mailForError"] = "0"
        <#Вызов функции "logit" с передачей ей в качестве параметра -message текства, что конкретно происходит указано ниже#>
        logIt -message "ERROR sending mail"
    }
}


<#Функция предназначена для логирования, то есть для записи успешных или неуспешных пунктов выполнения скрипта, что значительно облегчает отладку
пункты, то есть моменты записи логов необходимо указывать вызовом функции в нужных местах.#>
function logIt {
    param (
        <#Функция с одним параметром типа string#>
        [string]$message
    )
    <#При вызове функии и передаче ей текста происходит сохранение текущего времени в переменную, некий аналог timestamp#>
    $cur_date = [string](Get-Date -format 'yyyy-MM-dd HH:mm:ss,fff')
    <#Проверка на длину передаваемого текста для функции logIt и соответствии первых 5 символов в тексте с "ERROR"#>
    if (($message.Length -ge 5) -and ($message.Substring(0,5) -eq "ERROR")) {
        <#Если обе проверки успешны, тогда: создание объединение данных в единую строку из $cur_date (то есть timestamp) и значения полученных параметром $message 
        Так же подразумевается НЕКОЕ ПОДОБИЕ добавления выгрузки из переменной $Error, для добавление данных в строку, в которых должна указывать причина ошибки
        При этом используется некий текущий объект "$_" и его свойство ".ScriptStackTrace", не представляю как данная конструкция работает, могу улишь предположить,
        что по задумке должна выдаваться подробная информация об ошибке при текущем вызове функции "logIt"
        Либо добавляется информация об ошибках из внешнего приложения#>
        $output = ("$cur_date $message " + $_ + " " + $_.ScriptStackTrace)

        <#Проверка текущей xml-таблицы на значение свойства mailForError, если равно 1, тогда#>
        if ($emailSettings["mailForError"] -eq "1") {
            <#Вызов функции emailSend и передача параметру -body значения $output, то есть "timestamp; сообщение вида 'ERROR sending mail'; информация о причине возникшей
            ошибки"#>
            emailSend -body $output
        }
    <#В случае, если  сообщения типа 'ERROR sending mail' получено не было, сформировать текст вида "'timestamp' 'сообщение полученное функцией "logIt"'"#>
    } else {
        $output = "$cur_date $message"
    }
    <#Попытка записать свормированное сообщение в "logs\log_<timestamp>.log"#>
    try {  
        $output | Out-File -filepath $logFile -Append
    <#При любой возникшей ошибке назначить переменной $rest Значение $false#>
    } catch {
        $res = $false
    }
    <#Не совсем понимаю, для чего данная конструкция. По моему опыту "return $результат" используется для передачи каких либо результирующих данных на выход функции. Данная
    функция не подразумевает, что ей необходимо что-то передать по результатам своего выполнения. Так же return используется как явное указание функции закончить свое
    выполнение при своем появлении, например при отлавливании ошибок в try/catch. Здесь же и так находится конец функции и нет loop для ее повторного вызова. Да и return
    не находится внутри catch.#>
    return $res;
}

function dateExtract {
    <#Функция принимает единственный параметр date без указания формата данных#>
    param (
        $date
    )
    <#Объявление пустой переменной $ret#>
    $ret = ""
    <#Проверка на наличие любых данных в переменной $date
    Если $date не равна "", тогда:#>
    if ($date) {
        <#Если данные внутри $date соответствуют формату [datetime]#>
        if ($date.GetType() -eq [datetime]) {
            <#Тогда изменения типа данных [datetime] на [string] и сохранение даты в виде гггг-ММ-дд#>
            $ret = $date.ToString("yyyy-MM-dd")
        <#В любом ином случае:#>
        } else {
            <#Создание объекта $d с типа [datetime]#>
            $d = New-Object DateTime
            <#Сравнение полученных параметром данных с объектом $d, на предмет схожести (например данные переданы в формате строка)#>
            if ([datetime]::TryParse($date,[ref]$d)) {
                <#Если проверка пройдена, тогда смена формата с [datetime]$d на [string]$d
                Мне кажется здесь ошибка и вместо переменной $d необходимо изменить данные переменной $date, т.к. не вижу надобности в строке вида 0000-00-00 где либо#>
                $ret = $d.ToString("yyyy-MM-dd")
            }
        }
    }
    <#Закончить функцию и вернуть результат в виде даты#>
    return $ret
}

####################
### Begin script ###
####################

<#По моему мнению здесь ошибка, заменил бы на:
Не знаю при помощи чего ваша организация запускает ps скрипты, но явно, что не при помощи TaskSchd в windows, там данная конструкция работать не будет
Поэтому я бы использовал явное указание на родительский каталог

$settingsFile = (Join-Path -Path $PSScriptRoot -ChildPath "settings.xml")
#>

$settingsFile = "settings.xml"



<#Считывание .xml файла settings.xml и запись полученных данных в переменную $settings#>
[xml]$settings = Get-Content $settingsFile -Encoding UTF8

<#Далее объявление переменных со значениями взятыми из settings.xml#>
$identity = $settings.medi.identity;
$application = $settings.medi.application;
$cliendId = $settings.medi.clintId
$clientSecret = $settings.medi.clientSecret
$MediId = $settings.medi.mediId
$inbox = $settings.medi.inbox #from API to Folder
$outbox = $settings.medi.outbox #from Folder to API
$outboxProcessed = $settings.medi.outboxProcessed
$logFolder = $settings.medi.logFolder
$lastEventPointer = $settings.medi.lastEventPointer
$clinics = $settings.medi.clinics

<#Формирование пути до лог-файла#>
<#Я бы использовал здесь явное указание с использованием $PSScriptRoot

$logFile = (Join-Path -Path $PSScriptRoot -ChildPath ("$logFolder" + '\log_' + [string](Get-Date -format yyyy-MM-dd) + '.log'))
#>

[string]$logFile = "$logFolder" + '\log_' + [string](Get-Date -format yyyy-MM-dd) + '.log'

<#Формирование hashtable $emailSettings#>
$emailSettings = @{};
$emailSettings["mailForError"] = $settings.medi.emailSettings.mailForError;
$emailSettings["smtpHost"] = $settings.medi.emailSettings.smtpHost;
$emailSettings["smtpPort"] = $settings.medi.emailSettings.smtpPort;
$emailSettings["needAuth"] = $settings.medi.emailSettings.needAuth;
$emailSettings["needSSL"] = $settings.medi.emailSettings.needSSL;
$emailSettings["smtpUser"] = $settings.medi.emailSettings.smtpUser;
$emailSettings["smtpPassword"] = $settings.medi.emailSettings.smtpPassword;
$emailSettings["emails"] = $settings.medi.emailSettings.emails;
$emailSettings["sendFrom"] = $settings.medi.emailSettings.sendFrom;
$emailSettings["emailSubject"] = $settings.medi.emailSettings.emailSubject;


<#начальная запись в лог-файле#>
logIt -message "START";

<#добавление в лог строки "Load LoadPSD1.ps1"#>
logIt -message "Load LoadPSD1.ps1";


<#Попытка импорта скрипта LoadPSD1.ps1. Неудачный имопрт никак не отобразится ни в консоли, ни в логах (т.к. туда и не записывается), скрипт продолжит выполнение#>
try {
    <#$PSScriptRoot + "\ImportExcel-master\LoadPSD1.ps1"#>
    ImportExcel-master\LoadPSD1.ps1
}
catch {} 


# 1. get settings
<#Запуск командлета для работы с restapi, с данным продуктом я не работал, поэтому не могу сказать точно, что здесь происходит, но в чем я смог разобраться укажу далее#>

<#Происходит соединение с identity.testkontur.ru, получение от сервера первичных данных и добавление их в переменную $response для дальнейшей аутентификации#>
    try {
        $response = Invoke-RestMethod $identity -Method Get -Body $null -Headers $null;
        <#для каких-то целей происходит отдельная выгрузка token_endpoint в отдельную переменную, данная переменная используется одиножды и можно вполне обойтись строкой
        $response.token_endpoint в нужном месте.  #>
        $token_endpoint = $response.token_endpoint;
    } catch {
        <#в случае неудачи происходит создание двух записей в лог-файле и завершение работы скрипта#>
        logIt -message "ERROR";
        logIt -message "STOP"
        return
    }

# 2. get token
<#аутентификация пользователя#>
<#происходит формирование строки для дальнейшей аутентификации вида medi_test_client:xxx#>
    $auth_string = $cliendId + ":" + $clientSecret
<#происходит шифрование данной строки при помощи кодировки windows-1251 и форматирования строки в массив байтов #>
    $auth_string = [System.Text.Encoding]::GetEncoding("windows-1251").GetBytes($auth_string)
<#массив байтов форматируется при помощи base64 в строковое значение#>
    $auth_string = [Convert]::ToBase64String($auth_string)
    $auth_string = $auth_string.ToString()

<#создание переменной типа hashtable headers для использования в отсылке сообщения удаленному серверу
необходимо для получения http запросов, часто преобразуется в json#>
    $headers = @{
        "Authorization" = "Basic $auth_string"
        "Content-Type" = 'application/x-www-form-urlencoded'
    }
<#формирование тела сообщения#>
    $body = "grant_type=client_credentials&scope=medi_api"

<#не совсем понятно как конкретно
Но происходит получения токена на доступ при помощи зашифрованных логина:пароля
то есть происходит создание чего-то (post) при помощи логина:пароля для получения возмоности пройти аутентификацию #>
    try {
        $response = Invoke-RestMethod $token_endpoint -Method Post -Body $body -Headers $headers
        $access_token = $response.access_token
        $token_type = $response.token_type
    <#в случае неудачи просиходит запись в лог двух строк и завершение работы скрипта#>
    } catch {
        logIt -message "ERROR";
        logIt -message "STOP"
        return
    }

# 3. Get parties
<#формирование другого заголовка для отправки сообщения#>
    $headers = @{
        "Authorization" = "$token_type $access_token"
    }
<#тело сообщения пусто#>
    $body = $null

    <#форматирование адресной строки#>
    $method = "parties"
    $url = "$application$method/$MediId"

    <#попытка выполнить запрос типа get, то есть получение информации при помощи полученного токена
    адресс https://medi-api.testkontur.ru/V2/parties/test-insurer-kryachko@medi.kontur.ru
    #>
    try {
        $response = Invoke-RestMethod $url -Method Get -Body $body -Headers $headers

    } catch {
        <#создание строк в лог-файле и завершение работы скрипта#>
        logIt -message "ERROR нет доступа к организации или организация не существует";
        logIt -message "STOP"
        return
    }

# 4. get events
<#вход в цикл do/while, в котором происходит основное выполнения задач скрипта#>
    do {
        <#формирование заголовка#>
        $headers = @{
            "Authorization" = "$token_type $access_token"
        }
        $body = $null
        <#метов web приложения events#>
        $method = "events"

        <#создание hashtable params для получения каких-то данных
        в таблице указан id пользователя test-insurer-kryachko@medi.kontur.ru
        некое значение lastEventPointer вида 38593072-ba1a-11eb-8080-808080808080
        и ключ count
        #>
        $params = @{
            "mediId" = $MediId
            "lastEventPointer" = $lastEventPointer
            "count" = "1000"
        }

        <#объявление переменной $q_str со значением $null#>
        $q_str = $null
        <#для каждого из ключей в $params выполняется цикл foreach 
        #>
        foreach ($p in $params.Keys) {
            <#если $q_str не пусто:#>
            if ($q_str) {
                <#произвести добавление "&" к значению в $q_str, возможно как некий разделитель#>
                $q_str = $q_str + "&"
            <#в любом ином случае присвоить переменной значение "?", то есть в самый первый раз#>
            } else {
                $q_str = "?"
            }
            <#в итоге сформировать строку вида ?count=1000&mediId=test-insurer-kryachko@medi.kontur.ru&lastEventPointer=38593072-ba1a-11eb-8080-808080808080#>
            $q_str = $q_str + $p + "=" + $params[$p]
        }
        <#формирование итоговой http строки url
        https://medi-api.testkontur.ru/V2/events?count=1000&mediId=test-insurer-kryachko@medi.kontur.ru&lastEventPointer=38593072-ba1a-11eb-8080-808080808080
        #>
        $url = "$application$method$q_str"

        <#получение json данных у web-сервера#>
        try {
            $response = Invoke-RestMethod $url -Method Get -Body $body -Headers $headers

        <#логирование и завершение работы скрипта при возникновении ошибки#>
        } catch {
            logIt -message "ERROR";
            logIt -message "STOP"
            return
        }
        
        <#логироване строки response count=0 lastEventPointer=38593072-ba1a-11eb-8080-808080808080#>
        logIt -message ("response count=" + $response.count + " lastEventPointer=" + $response.lastEventPointer)

        <#После получения ответа от web сервера записать в лог все полученные события, точнее их id и timestamp, а не что-конкретно произошло#>
        foreach ($event in $response.events) {
            logIt -message ("event eventId=" + $event.eventId + " eventPointer=" + $event.eventPointer + " eventDateTime=" + $event.eventDateTime + " eventType=" + $event.eventType)

            <#проверка всех полученных событий на совпадению значению $event.eventType на NewInboxMessage/NewOutboxMessage/MessageDelivered/ProcessingTimesReport/
            MessageUndelivered/MessageCheckedByRecipient
            В случае совпадения по типу просходит:#>
            switch ($event.eventType) {

                "NewInboxMessage" {
                    <#формирование url при помощи id сообщения и указанного "метода web-сервера"#>
                    $messageId = $event.eventContent.fullMessageMeta.messageId
                    $method = "messages"
                    $url = "$application$method/$messageId"

                    <#попытаться выполнить запрос get к сформированному url#>
                    try {
                        $message = Invoke-RestMethod $url -Method Get -Headers $headers
                    } catch {
                        logIt -message "ERROR";
                        logIt -message "STOP"
                        return
                    }
                    <#выгрузка полученного ответа от сервера в $content#>
                    $content = $message.data.content
                    <#запуск процесса дешифрования#>
                    <#конвертация из битового массива#>
                    $content = [Convert]::FromBase64String($content)
                    <#применение кодировки#>
                    $content = [System.Text.Encoding]::GetEncoding("UTF-8").GetString($content)
                    <#замена в полученном timestamp от сервера ненужных символов для дальнейшего формирвоания названия конечного файла#>
                    $fileName = ($event.eventDateTime.Replace(":","-").Replace(".","-") + "_" + $messageId + ".xml")
                    <#сохранение полученного сообщения в каталог inbox_sk в корне скрипта#>
                    $content | Out-File -filepath "$inbox/$fileName" -Encoding utf8

                <#далее к $lastEventPointer = $response.lastEventPointer#>#>
                }
                <#при совпадении: проследовать далее к $lastEventPointer = $response.lastEventPointer#>
                "NewOutboxMessage" {
                }
                <#при совпадении: проследовать далее к $lastEventPointer = $response.lastEventPointer#>
                "MessageDelivered" {
                }
                <#при совпадении: проследовать далее к $lastEventPointer = $response.lastEventPointer#>
                "ProcessingTimesReport" {
                }
                <#При совпадении типа сообщения получить id, указать $method, сформировать url#>
                "MessageUndelivered" {
                    $messageId = $event.eventContent.basicMessageMeta.messageId
                    $method = "messages"
                    $url = "$application$method/$messageId"

                    <#попытаться выполнить запрос get к сформированному url #>
                    try {
                        $message = Invoke-RestMethod $url -Method Get -Headers $headers
                    } catch {
                        logIt -message "ERROR";
                        logIt -message "STOP"
                        return
                    }
                <#далее к $lastEventPointer = $response.lastEventPointer#>#>
                }
                <#Проверка на совпадение типа события#>
                "MessageCheckedByRecipient" {
                    <#Если статуc event.eventContent.checkingStatus неуспешен#>
                    if ($event.eventContent.checkingStatus -eq "Fail") {
                        <#формируем id#>
                        $messageId = $event.eventContent.messageId
                        <#явно указываем метов#>
                        $method = "messages"
                        <#на основе id и метода формируем url#>
                        $url = "$application$method/$messageId"

                        <#пытаемся произвести запрос get по сформированному url#>
                        try {
                            $message = Invoke-RestMethod $url -Method Get -Headers $headers
                        } catch {
                            logIt -message "ERROR";
                            logIt -message "STOP"
                            return
                        }
                        
                    }
                   
                }
            } 
        }
        <#после прверки switch явно указываем последний pointer (id) в виде переменной#>
        $lastEventPointer = $response.lastEventPointer
    <#цикл выполняется до тех пор, пока $response.count > 0
    мне не ясен принцип подсчета этого самого $response.count, предположу, что как то вычитается web-сервером, иначе произойдет петля и скрипт не сможет выполниться далее#>
    } while ($response.count -gt 0)

    <#явно указываем свойству в выгруженном в память .xvl $settings.medi.lastEventPointer значение $response.lastEventPointer
    Не знаю, зачем. Т.к. д#>
    $settings.medi.lastEventPointer = $response.lastEventPointer
   
    <#Сохраняем измененный выгруженный в память файл .xml в файл settings.xml#>
    $settings.Save($settingsFile)

# 5.0 Get partners
    <#формирование нового заголовка#>
    $headers = @{
        "Authorization" = "$token_type $access_token"
    }

    $body = $null

    $method = "parties"

    $url = "$application$method/$MediId/partners"

    <#пытаемся произвести запрос get, для более развернутого ответа нет информации, т.к. тестовая УЗ не производит никаких подключений#>
    try {
        $partners = Invoke-RestMethod $url -Method Get -Body $body -Headers $headers
        
    } catch {
        logIt -message "ERROR нет доступа к организации или организация не существует";
        logIt -message "STOP"
        return
    }

# 5. post message
    <#формирвоание заголовка для записи данных на сервер, запрос post#>
    $headers = @{
        "Authorization" = "$token_type $access_token"
        "Content-Type" = "application/json; charset=utf-8"
    }
    $method = "messages"
    $url = "$application$method"
    
    <#выгружаем список всего содержимого из каталога outbox_sk находящемся в корне скрипта в переменную#>
    <#Если по какой-либо причине пользователь добавит файлам статус "скрытые", данные файлв не попадут в список. Не буду моделировать ситуации и в каком случае они могут
    возникнуть, но суть неизменна. Поэтому я бы добавил пол строки кода, чтобы навсегда избежать возможной проблемы
    
    $files = Get-ChildItem -Path $outbox -Attributes !D, !D+H, !D+H+!S
    #>

    $files = Get-ChildItem -Path $outbox

    <#производим цикл для каждого файла из полученного выше массива#>
    foreach ($file in $files) {
        <#создаем записи в лог-файле взяв за основу данные самих файлов#>
        logIt -message (" FullName=" + $file.FullName + " Extension=" + $file.Extension + " Attributes=" + $file.Attributes + " Mode=" + $file.Mode)
        <#форматируем расширение файла нижним регистром и проверяем на соответствие с ".xml"#>
        if ($file.Extension.ToLower() -eq ".xml") {
            
            <#создаем переменную и выгружаем в нее все содержимое файла в кодировке UTF8#>
            $message = Get-Content $file.FullName -Encoding UTF8
            <#при помощи класса [System.Text.Encoding] преобразуем все содержимое файла в массив байтов#>
            $message = [System.Text.Encoding]::GetEncoding("utf-8").GetBytes($message)
            <#шифруем при помощи Base64 полученный массив в строку#>
            $message = [Convert]::ToBase64String($message)
            <#формируем body json файла#>
            $bodyJson = @{
                "mediId" = $mediId
                "message" = $message
            }
            $body = $bodyJson
            <#конвертируем шифрованное содержимое в json#>
            $body = $body|ConvertTo-Json

            <#формируем название при помощи видоизмененного timestamp и имени файла#>
            $savedFileName = (Get-Date).ToString("o").Replace(":","-").Replace(".","-").Replace("+","-") + "_" + $file.Name;

            <#Пытаемя отправить содержимое на сервер#>
            try {
                $response = Invoke-RestMethod $url -Method Post -Body $body -Headers $headers

                <#логируем событие#>
                logIt -message ("Message sent to mediId=" + $response.mediId + " messageId=" + $response.messageId + " documentCirculationId=" + $response.documentCirculationId + " from " + $file.Name + ", moved to " + $outboxProcessed + "\" + $savedFileName)
            } catch {
                logIt -message "ERROR";
                logIt -message "STOP"
                return
            }
            <#меняем местоположение обработанного и отправленного файла в каталог 'outbox_sk\outboxProcessed', расположенный в корне скрипта#>
            Move-Item $file.FullName -Destination ($outboxProcessed + "\" + $savedFileName) -Force
        }
        <#производим проверку на соответствие расширения файла ".xlsx"#>
        if ($file.Extension.ToLower() -eq ".xlsx") {

            <#для дальнейшего использвоания формируем hashtable с парамептрами fullname файла, noheader, и название страницы как "Список застрахованных"#>
            $ExcelParams = @{
                            Path    = $file.FullName
                            NoHeader = $true
                            WorksheetName = "Список застрахованных"
                        }
            
            <#при помощи модуля Import-Excel загружаем worksheet "Список застрахованных"#>
            $excel = Import-Excel @ExcelParams
            
            <#полный путь файла конвертируем в нижний регистр и добавляем в переменную $FileName#>
            $FileName = ($file.FullName).ToLower()
            
            <#проводим проверку на содержание в пути файла строки "открепление"#>
            if ($FileName.Contains("открепление")) {
                <#если проверка успешна, объявляем переменную с указанным значением#>
                $ListFunction = "Deletion"
            }

            <#в случае если путь до файла содержит строку "изменение"#>
            elseif ($FileName.Contains("изменение")) {
                <#создаем переменную со значением $true#>
                $ListFunctionIsChange = $true

                <#загоняем в переменные $PersonalInfoArrayChange значения указанных столбцов 10 строки файла#>
                $PersonalInfoArrayChange = ($excel[9].P1, $excel[9].P2, $excel[9].P3, $excel[9].P4, $excel[9].P5, $excel[9].P14, $excel[9].P15)
                $PersonalInfoArrayOriginal = ($excel[9].P20, $excel[9].P21, $excel[9].P22, $excel[9].P23, $excel[9].P24, $excel[9].P33, $excel[9].P34)

                <#формируем строку из значений, добавляя после каждого значения запятую
                сравниваем получившиеся строки друг с другом#>
                if (($PersonalInfoArrayChange -join ",") -eq ($PersonalInfoArrayOriginal -join ",")) {
                    <#Если строки полностью совпадают изменяем значение для указанной переменной#>
                    $ListFunction = "InsuranceProgramChange"
                }
                <#в любом другом случае задаем значение переменной#>
                else {
                    $ListFunction = "PersonalInfoChange"
                }
            }
            <#Если ни одна из предыдущих проверок неуспешна#>
            else {
                <#задаем значение переменной#>
                $ListFunction = "Addition"
            }

            <#присваиваем переменной значение равное 6строке 5 таблицы #>
            $clinicINN = $excel[5].P5

            <#формируем массив данных по отфильтрованному $clinicINN#>
            $myPartner = @($partners | Where-Object{$_.partyInfo.inn -eq $clinicINN})

            <#Если совпадений нет#>
            if ($myPartner.Count -eq 0) {
                <#логируем строку ниже#>
                logIt -message ("ERROR Не нашли подходящего партнера с таким ИНН $clinicINN, файл " + $file.FullName + " не обработан");

                <#на мой взгляд строка continue не нужна#>
                continue;
            }
            <#если количество объектов в массиве не равно 1#>
            if ($myPartner.Count -ne 1) {
                <#логируем строку ниже#>
                logIt -message ("ERROR Нашли более одного партнера с таким ИНН $clinicINN, файл " + $file.FullName + " не обработан");

                <#строка не нужна#>
                continue;
            }

            <#назначаем переменную со значением id первого объекта в массиве $myPartner#>
            $clinicMediId = $myPartner[0].partyInfo.mediId


            <#формируем xml сообщение из данных excel и данных полученных при подключении к серверу#>
            $out_xml = '<eDIMessage id="messageId">
	            <!-- messageId - уникальный идентификатор отправляемого сообщения, формируемый на стороне отправителя, на которомы впоследствии придут статусные сообщения о результате доставки/обработки у провайдера/контрагента-получателя данного сообщения. -->
	            <interchangeHeader>
		            <sender>' + $MediId + '</sender>
		            <!-- MediId СК -->
		            <recipient>' + $clinicMediId + '</recipient>
		            <!-- MediId ЛПУ -->
		            <documentType>RELIST</documentType>
	            </interchangeHeader>
            '
            $out_xml += '
            	<insurantsList
                  insurantsListNumber="' + $excel[5].P9 + '"
                  insurantsListDate="' + (dateExtract -date $excel[5].P10) + '">
                <insurantsListFunction>' + $ListFunction + '</insurantsListFunction>
            '
            $out_xml += '
            	<contractIdentificator
                  number="' + $excel[5].P7 + '"
                  date="' + (dateExtract -date $excel[5].P8) + '"/>
            '
            $out_xml += '
		            <insuranceCompany>
			            <!-- страховая -->
			            <mediId>' + $MediId + '</mediId>
			            <!-- MediId СК -->
			            <organization>
				            <name>' + $excel[5].P1 + '</name>
				            <inn>' + $excel[5].P2 + '</inn>
				            <kpp>' + $excel[5].P3 + '</kpp>
			            </organization>
		            </insuranceCompany>
		            <healthCareProvider>
			            <!-- ЛПУ -->
			            <mediId>' + $clinicMediId + '</mediId>
			            <!-- MediId ЛПУ -->
			            <organization>
				            <name>' + $excel[5].P4 + '</name>
				            <inn>' + $excel[5].P5 + '</inn>
				            <kpp>' + $excel[5].P6 + '</kpp>
			            </organization>
		            </healthCareProvider>
            '

            <#назначаем переменную, по принципу DRY - 1 (точка изменения всех данных)
            сделано, чтобы не менять вручную каждый раз номер строки файла excel, если ее будет необходимо поменять#>
            $i = 9

            $out_xml += '
		            <listOfInsurants>
			            <!-- список застрахованных -->
            '
            <#проверка на пустую ячейку#>
            while (($excel[$i].P1 + '') -ne '') {
                
                $dateCancel = $excel[$i].P8
                if ($excel[$i].P9) {
                    $dateCancel = $excel[$i].P9
                }

                <#далее так же формируем xml, без знания в каком виде нужно получить конечной результат довольно долго "дешифровать" данный код, поэтому я воздержусь
                Глановное, что необходимо получить готовый xml который формируется в зависимости от нескольких проверок указанных ниже#>
                if ($ListFunctionIsChange) {$out_xml += '<insurant insuredPersonStatus="Change">'}
                else {$out_xml += '<insurant>'}
                $out_xml += '
				                <!-- для каждого застрахованного такой отдельный тег -->
				                <fullName>
					                <lastName>' + $excel[$i].P1 + '</lastName>
					                <firstName>' + $excel[$i].P2 + '</firstName>
					                <middleName>' + $excel[$i].P3 + '</middleName>
				                </fullName>
				                <dateOfBirth>' + (dateExtract -date $excel[$i].P5) + '</dateOfBirth>
				                <!-- дата в формате YYYY-MM-DD -->
				                <gender>'
                if (@("M","m","М","м").Contains($excel[$i].P4[0].ToString())) {$out_xml += 'Male'} else {$out_xml += 'Female'}
                $out_xml += '</gender>
				                <!-- Male / Female -->
				                <employer>
					                <!-- работодатель -->
					                <mediId/>
					                <organization>
						                <name>' + $excel[$i].P11 + '</name>
						                <inn/>
						                <kpp/>
					                </organization>
				                </employer>'
                $out_xml += '<insurancePolicy>
					                <number>' + $excel[$i].P6 + '</number>
					                <!-- номер полиса страховой -->
					                <fromDate>' + (dateExtract -date $excel[$i].P7) + '</fromDate>
					                <toDate>' + (dateExtract -date $excel[$i].P8) + '</toDate>'
                if ($ListFunction -eq "Deletion") {$out_xml += '<bindingEndDate>' + (dateExtract -date $dateCancel) + '</bindingEndDate>'}

                if ($clinicMediId -eq "medsi-clinic@medi.kontur.ru") {
                    $insuranceProgramCode = $excel[$i].P17 + '_' + $excel[$i].P16 + '_' + $excel[$i].P19 + '_' + $excel[5].P7 + '_'
                }
                else {
                    $insuranceProgramCode = $excel[$i].P17 + '_' + $excel[$i].P16 + '_' + $excel[$i].P19 + '_'
                } 
                $out_xml += '</insurancePolicy>
                                <insuranceProgram>
					                <name>' + $excel[$i].P16 + '</name>
					                <code>' + $insuranceProgramCode + '</code>
					                <programLocationCode>' + $excel[$i].P19 + '</programLocationCode>
					                <comment>' + $excel[$i].P16 + '</comment>
					                <contractIdentificator number="' + $excel[5].P7 + '" date="' + (dateExtract -date $excel[5].P8) + '"/>
                                    <paymentOptions>' + $excel[$i].P10 + '</paymentOptions>
					                <supplementaryAgreementIdentificator number="' + $excel[$i].P12 + '" date="' + (dateExtract -date $excel[$i].P13) + '"/>
				                </insuranceProgram>'
                $out_xml += '<contacts><!-- контакты застрахованного -->
                                    <cellPhone>' + $excel[$i].P14 + '</cellPhone>
                                    <homePhone></homePhone>
                                    <privateEmail></privateEmail>
                                </contacts>'
                $out_xml += '<russianAddress>
					                <fullAddress>' + $excel[$i].P15 + '</fullAddress>
				             </russianAddress>
			                </insurant>'

                if ($ListFunctionIsChange) {
                    $out_xml += '
                            <insurant insuredPersonStatus="Original">
				                <!-- для каждого застрахованного такой отдельный тег -->
				                <fullName>
					                <lastName>' + $excel[$i].P20 + '</lastName>
					                <firstName>' + $excel[$i].P21 + '</firstName>
					                <middleName>' + $excel[$i].P22 + '</middleName>
				                </fullName>
				                <dateOfBirth>' + (dateExtract -date $excel[$i].P24) + '</dateOfBirth>
				                <!-- дата в формате YYYY-MM-DD -->
				                <gender>'
                if (@("M","m","М","м").Contains($excel[$i].P23[0].ToString())) {$out_xml += 'Male'} else {$out_xml += 'Female'}
                $out_xml += '</gender>
				                <!-- Male / Female -->
				                <employer>
					                <!-- работодатель -->
					                <mediId/>
					                <organization>
						                <name>' + $excel[$i].P30 + '</name>
						                <inn/>
						                <kpp/>
					                </organization>
				                </employer>'
                $out_xml += '<insurancePolicy>
					                <number>' + $excel[$i].P25 + '</number>
					                <!-- номер полиса страховой -->
					                <fromDate>' + (dateExtract -date $excel[$i].P26) + '</fromDate>
					                <toDate>' + (dateExtract -date $excel[$i].P27) + '</toDate>'

                if ($clinicMediId -eq "medsi-clinic@medi.kontur.ru") {
                    $insuranceProgramCodeChange = $excel[$i].P36 + '_' + $excel[$i].P35 + '_' + $excel[$i].P38 + '_' + $excel[5].P7 + '_'
                }
                else {
                    $insuranceProgramCodeChange = $excel[$i].P36 + '_' + $excel[$i].P35 + '_' + $excel[$i].P38 + '_'
                } 
                
                $out_xml += '</insurancePolicy>
                                <insuranceProgram>
					                <name>' + $excel[$i].P35 + '</name>
					                <code>' + $insuranceProgramCodeChange + '</code>
					                <programLocationCode>' + $excel[$i].P38 + '</programLocationCode>
					                <comment>' + $excel[$i].P35 + '</comment>
					                <contractIdentificator number="' + $excel[5].P7 + '" date="' + (dateExtract -date $excel[5].P8) + '"/>
                                    <paymentOptions>' + $excel[$i].P29 + '</paymentOptions>
					                <supplementaryAgreementIdentificator number="' + $excel[$i].P31 + '" date="' + (dateExtract -date $excel[$i].P32) + '"/>
				                </insuranceProgram>'
                $out_xml += '<contacts><!-- контакты застрахованного -->
                                    <cellPhone>' + $excel[$i].P33 + '</cellPhone>
                                    <homePhone></homePhone>
                                    <privateEmail></privateEmail>
                                </contacts>'
                $out_xml += '<russianAddress>
					                <fullAddress>' + $excel[$i].P34 + '</fullAddress>
				                </russianAddress>
			                </insurant>'

                }

                $i++
            }
            $out_xml += '
		            </listOfInsurants>
	            </insurantsList>
            </eDIMessage>'

#            $out_xml | Out-File -filepath "test.xml" -Encoding utf8
#            continue


            <#перменной $message изменяем значение на сформированный $out_xml#>
            $message = $out_xml
            
            <#кодируем, шифруем#>
            $message = [System.Text.Encoding]::GetEncoding("utf-8").GetBytes($message)
            $message = [Convert]::ToBase64String($message)

            <#формируем json#>
            $bodyJson = @{
                "mediId" = $mediId
                "message" = $message
            }
            $body = $bodyJson
            $body = $body|ConvertTo-Json

            <#формируем имя файла по datestamp и имени файла#>
            $savedFileName = (Get-Date).ToString("o").Replace(":","-").Replace(".","-").Replace("+","-") + "_" + $file.Name;

            <#Пытаемcя отправить сформированный json с шифрованным содержимым на сервер#>
            try {
                $response = Invoke-RestMethod $url -Method Post -Body $body -Headers $headers
                
                <#логируем#>
                logIt -message ("Message sent from mediId=" + $response.mediId + " messageId=" + $response.messageId + " documentCirculationId=" + $response.documentCirculationId + " from " + $file.Name + ", moved to " + $outboxProcessed + "\" + $savedFileName)
            } catch {
                logIt -message "ERROR";
                logIt -message "STOP"
                return
            }
            <#перемещаем файл#>
            Move-Item $file.FullName -Destination ($outboxProcessed + "\" + $savedFileName) -Force

        }
        <#проводим проверку расширения на предмет старого формата excel и ничего не делаем в любом случае#>
        if ($file.Extension.ToLower() -eq ".xls") {

        }

    }
    <#логируем строку STOP#>
    logIt -message "STOP";
    <#явное указание на завершене работы скрипта#>
    exit

